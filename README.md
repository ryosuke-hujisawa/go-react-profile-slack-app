# go-react-profile-slack-app

```
スラックのプロフィール欄に入力するurl

```

```
・フロントエンド
Babel
webpack
ES6
React

・バックエンド
go

```

```
$ node -v
v10.15.1

$ npm -v
6.4.1
```

環境構築時の参考リファレンス
[Babelとwebpackを使ってES6でReactを動かすまでのチュートリアル](https://qiita.com/akirakudo/items/77c3cd49e2bf39da79dd)
